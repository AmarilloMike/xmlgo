package common

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
)

// ParseFile - parses xml address file
func ParseFile(fileNamePath string) AddressBatch {

	xmlFile, err := os.Open(fileNamePath)

	var addr AddressBatch

	if err != nil {
		fmt.Println("File Name: ", xmlFile)
		fmt.Println("Error opening file:")
		fmt.Println("Error Message:", err)
		panic(err)
	}

	defer xmlFile.Close()

	b, _ := ioutil.ReadAll(xmlFile)

	xml.Unmarshal(b, &addr)

	return addr

}
