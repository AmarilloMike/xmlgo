package common

/*
In this example, tags are used to read xml data into
structure elements named different from the xml
elements.

Note: This example also shows an additional field which
is appended to the structure and is NOT read from the
xml file. See AddrHdr.CityState below. Although this
field is not populated from xml, it does not affect
the successful xml read process. The extra field is
therefore available for use in the program.
*/

// AddressBatch - Xml Root - Parent struct
type AddressBatch struct {
	Address []AddressElement `xml:"Address"`
}

// AddressElement - Encompasses all information needed
// for an address
type AddressElement struct {
	AddrHdr   AddrHdr          `xml:"AddrHdr"`
	AddrLines []AddrLinesArray `xml:"AddrLines"`
}

// AddrHdr - Contains basic single string elements of
// an address
type AddrHdr struct {
	Name       string `xml:"Name"`
	LastName   string `xml:"LastName"`
	FirstName  string `xml:"FirstName"`
	MiddleName string `xml:"MiddleName"`
	City       string `xml:"City"`
	State      string `xml:"State"`
	ZipCode    int    `xml:"ZipCode"`
	// This field is NOT populated from the xml file!
	CityState string
}

// AddrLinesArray - Contains all the address lines
// associated with a given address
type AddrLinesArray struct {
	AddrLine []string `xml:"AddrLine"`
}
