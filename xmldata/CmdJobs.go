package common

// CommandsBatch - Xml Root and Parent Element
type CommandsBatch struct {
	CmdJobsHdr CommandJobsHdr `xml:"CommandJobsHeader"`
	CmdJobs    []CommandJob   `xml:"CommandJobs"`
}

// CommandJobsHdr - Holds base info related to
// command jobs
type CommandJobsHdr struct {
	LogFileRetentionInDays int    `xml:"LogFileRetentionInDays"`
	CmdExeDir              string `xml:"CommandExeDirectory"`
	LogPathFileName        string `xml:"LogPathFileName"`
}

// CommandJob - Holds command job information
// for a single command job
type CommandJob struct {
	CmdDisplayName             string  `xml:"CommandDisplayName"`
	CmdDescription             string  `xml:"CommandDescription"`
	CmdType                    string  `xml:"CommandType"`
	ExeCmdInDir                string  `xml:"ExecuteCmdInDir"`
	DelayCmdStart              string  `xml:"DelayCmdStartSeconds"`
	StartCmd                   string  `xml:"StartCmdDateTime"`
	KillJobsOnExitCodeGreater  int     `xml:"KillJobsOnExitCodeGreaterThan"`
	KillJobsOnExitCodeLessThan int     `xml:"KillJobsOnExitCodeLessThan"`
	CmdTimeOut                 float64 `xml:"CommandTimeOutInMinutes"`
	CmdElements                []CommandElement `xml:"CmdElements"`
}

// CommandElement - Holds one element of an
// executable command
type CommandElement struct {
	CmdUnit string `xml:"CmdElement"`
}
