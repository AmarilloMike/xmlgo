package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"

	common "bitbucket.org/xmlgo/00_ReadSimple/common"
)

func main() {

	xmlFile := "./simpleread.xml"
	fmt.Println("=======================================")
	fmt.Println("XML File: ", xmlFile)
	fmt.Println("=======================================")

	fmt.Println("=======================================")
	fmt.Println("==============DATA=====================")

	batch := parseFile(xmlFile)
	fmt.Println("Car 1")

	for i, car := range batch.Cars.Car {

		fmt.Println("Index", i, "Make:", car.Make, "\tModel:", car.Model, "  \tYear:", car.Year)
	}

}

func parseFile(xmlFilePath string) common.CarBatch {
	var dat common.CarBatch

	xmlFile, er1 := os.Open(xmlFilePath)

	if er1 != nil {
		fmt.Println("========== File Open ERROR ===================")
		fmt.Println("Error Opening File!")
		panic(er1)
	}

	defer xmlFile.Close()

	b, er2 := ioutil.ReadAll(xmlFile)

	if er2 != nil {
		fmt.Println("========== XML Read ERROR ===================")
		fmt.Println("Error Reading XML File!")
		panic(er2)
	}

	xml.Unmarshal(b, &dat)

	return dat
}
