package common

// CarBatch - Parent Structure for XML Data
type CarBatch struct {
	Cars CarsArray
}

type CarsArray struct {
	Car []CarElement
}

type CarElement struct {
	Make  string
	Model string
	Year  int
}
