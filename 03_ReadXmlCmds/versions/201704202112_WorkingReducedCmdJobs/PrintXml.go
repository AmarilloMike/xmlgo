package common

import "fmt"

// PrintXML - Prints Commands generated
// by reading XML file
func PrintXML(cmds CommandsBatch) {

	fmt.Println("=======================================")
	fmt.Println("Command Data from XML File")

	PrintCmdJobsHdr(cmds)
	PrintCmdJobs(cmds)

	return
}

// PrintCmdJobsHdr - Prints the Command
// Jobs Header info from CommandsBatch
// structure
func PrintCmdJobsHdr(cmds CommandsBatch) {

	fmt.Println("=======================================")
	fmt.Println("CmdJobsHdr")
	fmt.Println("=======================================")
	fmt.Println("LogFileRetentionInDays:", cmds.CmdJobsHdr.LogFileRetentionInDays)
	fmt.Println("CommandExeDirectory:", cmds.CmdJobsHdr.CmdExeDir)
	fmt.Println("LogPathFileName:", cmds.CmdJobsHdr.LogPathFileName)

	return
}

// PrintCmdJobs - Prints All Command Jobs
func PrintCmdJobs(cmds CommandsBatch) {
	fmt.Println("=======================================")

	fmt.Println("Printing Command Jobs")
	fmt.Println("=======================================")

	for _, cmdJob := range cmds.CmdJobs.CmdJobArray {
		fmt.Println("Display Name:", cmdJob.CmdDisplayName)
		fmt.Println("Command Desc:", cmdJob.CmdDescription)
		fmt.Println("Command Type:", cmdJob.CmdType)
		fmt.Println("ExecuteCmdInDir:", cmdJob.ExeCmdInDir)
		fmt.Println("StartCmdDateTime:", cmdJob.StartCmdDateTime)
		fmt.Println("KillJobsOnExitCodeGreaterThan:", cmdJob.KillJobsOnExitCodeGreaterThan)
		fmt.Println("KillJobsOnExitCodeLessThan:", cmdJob.KillJobsOnExitCodeLessThan)
		fmt.Println("CommandTimeOutInMinutes:", cmdJob.CommandTimeOutInMinutes)
		for _, cmdFragments := range cmdJob.CmdElements {
			for _, cmdUnit := range cmdFragments.CmdElements {
				fmt.Println("CmdElement:", cmdUnit)
			}

		}

		fmt.Println("=======================================")
	}
}
