package common

// CommandsBatch - Xml Root and Parent Element
type CommandsBatch struct {
	CmdJobsHdr CommandJobsHdr    `xml:"CommandJobsHeader"`
	CmdJobs    []CommandJobArray `xml:"CommandJobs"`
}

// CommandJobsHdr - Holds base info related to
// command jobs
type CommandJobsHdr struct {
	LogFileRetentionInDays int    `xml:"LogFileRetentionInDays"`
	CmdExeDir              string `xml:"CommandExeDirectory"`
	LogPathFileName        string `xml:"LogPathFileName"`
}

// CommandJobArray - Holds individual
// CommandJob structs
type CommandJobArray struct {
	CmdJobArray []CmdJob `xml:"CommandJob"`
}

// CmdJob - Command Job information
type CmdJob struct {
	CmdDisplayName                string                 `xml:"CommandDisplayName"`
	CmdDescription                string                 `xml:"CommandDescription"`
	CmdType                       string                 `xml:"CommandType"`
	ExeCmdInDir                   string                 `xml:"ExecuteCmdInDir"`
	DelayCmdStart                 string                 `xml:"DelayCmdStartSeconds"`
	StartCmdDateTime              string                 `xml:"StartCmdDateTime"`
	KillJobsOnExitCodeGreaterThan int                    `xml:"KillJobsOnExitCdeGreaterThan"`
	KillJobsOnExitCodeLessThan    int                    `xml:"KillJobsOnExitCdeLessThan"`
	CommandTimeOutInMinutes       float64                `xml:"CmdTimeOutInMinutes"`
	CmdElements                   []CommandElementsArray `xml:"CmdElements"`
}

// CommandElementArray - Holds CmdElement structures
type CommandElementsArray struct {
	CmdElements []string `xml:"CmdElement"`
}
