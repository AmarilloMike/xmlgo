package main

import "bitbucket.org/xmlgo/03_ReadXmlCmds/common"

func main() {

	xmlFile := "../common/cmdrGoCmds001.xml"

	var cmds common.CommandsBatch

	cmds = common.ParseXML(xmlFile)

	common.AssembleCmdElements(&cmds)

	common.PrintXML(cmds)

}
