package main

import (
	"fmt"

	common "bitbucket.org/xmlgo/01_ReadAddrs/common"
)

func main() {

	xmlFile := "../common/addresses.xml"
	fmt.Println("=======================================")
	fmt.Println("XML File: ", xmlFile)
	fmt.Println("=======================================")
	var a common.AddressBatch
	a = common.ParseFile(xmlFile)

	for i, addr := range a.Address {
		fmt.Println("=======================================")
		fmt.Println(i, addr.AddrHdr.Name)
		for _, addrlines := range addr.AddrLines {
			for _, addrline := range addrlines.AddrLine {
				fmt.Println(addrline)
			}

		}
		fmt.Println(addr.AddrHdr.City, ", ", addr.AddrHdr.State)
		fmt.Println("=======================================")
	}

}
