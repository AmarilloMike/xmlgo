package common

type AddressBatch struct {
	Address []AddressElement
}

type AddressElement struct {
	AddrHdr   AddrHdr
	AddrLines []AddrLinesArray
}

type AddrHdr struct {
	Name       string
	LastName   string
	FirstName  string
	MiddleName string
	City       string
	State      string
	ZipCode    int
}

type AddrLinesArray struct {
	AddrLine []string
}
